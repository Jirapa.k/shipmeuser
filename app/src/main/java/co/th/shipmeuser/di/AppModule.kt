package co.th.shipmeuser.di

import co.th.shipmeuser.network.api.createCallService
import co.th.shipmeuser.network.pref.pref.SharedPreference
import co.th.shipmeuser.network.repository.*
import co.th.shipmeuser.ui.authen.AuthenViewModel
import co.th.shipmeuser.ui.main.basket.BasketViewModel
import co.th.shipmeuser.ui.main.home.HomeViewModel
import co.th.shipmeuser.ui.main.order.OrderViewModel
import co.th.shipmeuser.ui.main.profile.ProfileViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {

    single { createCallService() }
    single { SharedPreference(get()) }
    single { AuthenRepository(get(), get(), get()) }
    single { HomeRepository(get(), get()) }
    single { ProfileRepository(get(), get()) }
    single { OrderRepository(get(), get()) }
    single { BasketRepository(get(), get()) }
    viewModel { AuthenViewModel(get()) }
    viewModel { HomeViewModel(get()) }
    viewModel { OrderViewModel(get()) }
    viewModel { ProfileViewModel(get()) }
    viewModel { BasketViewModel(get()) }
}