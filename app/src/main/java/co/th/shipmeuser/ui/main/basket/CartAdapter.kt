package co.th.shipmeuser.ui.main.basket

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import co.th.shipmeuser.R
import co.th.shipmeuser.databinding.CartItemBinding
import co.th.shipmeuser.network.models.cart.getCart.Product
import kotlinx.android.synthetic.main.cart_item.view.*

class CartAdapter(val onClick: (Product) -> Unit) :
    RecyclerView.Adapter<CartAdapter.ViewHolder>() {

    var dataList: ArrayList<Product> = arrayListOf()

    override fun getItemCount(): Int = dataList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: CartItemBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.cart_item, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(dataList[position])
        holder.itemView.iv_delete.setOnClickListener { onClick(dataList[position]) }
    }

    class ViewHolder(private val binding: CartItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindItem(data: Product) {
            binding.model = data
        }
    }

    fun delete(product: Product) {
        dataList.remove(product)
        notifyDataSetChanged()
    }
}