package co.th.shipmeuser.ui.main.basket

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import co.th.shipmeuser.R
import co.th.shipmeuser.databinding.ProductPreviewItemBinding
import co.th.shipmeuser.network.models.cart.comfirm.Products

class ProductPreviewAdapter :
    RecyclerView.Adapter<ProductPreviewAdapter.ViewHolder>() {

    var dataList: List<Products> = arrayListOf()

    override fun getItemCount(): Int = dataList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: ProductPreviewItemBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.product_preview_item, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(dataList[position])
        //  holder.itemView.iv_delete.setOnClickListener { onClick(dataList[position]) }
    }

    class ViewHolder(private val binding: ProductPreviewItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindItem(data: Products) {
            binding.model = data
        }
    }
}