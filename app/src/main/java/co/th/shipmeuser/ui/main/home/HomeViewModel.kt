package co.th.shipmeuser.ui.main.home

import android.util.Log
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import co.th.shipmeuser.network.models.cart.AddCartRequest
import co.th.shipmeuser.network.models.cart.AddCartResponse
import co.th.shipmeuser.network.models.home.CategoriesResponse
import co.th.shipmeuser.network.models.home.ManufacturerResponse
import co.th.shipmeuser.network.models.home.ModelResponse
import co.th.shipmeuser.network.models.products.Color
import co.th.shipmeuser.network.models.products.Datum
import co.th.shipmeuser.network.models.products.ProductResponse
import co.th.shipmeuser.network.models.products.Size
import co.th.shipmeuser.network.repository.HomeRepository

class HomeViewModel(private val repo: HomeRepository) : ViewModel() {
    var categoriesList: MutableLiveData<List<String>> = MutableLiveData()
    var manufacturersList: MutableLiveData<List<String>> = MutableLiveData()
    var modelList: MutableLiveData<List<String>> = MutableLiveData()
    var quantity: ObservableField<String> = ObservableField("1")
    var addToCartRequest = AddCartRequest()

    val colorProductAdapter = ColorsImageAdapter {
        Log.d("colorSelected", "" + it)
    }
    val productsAdapter = ProductsAdapter()
    val sizeAdapter = SizeAdapter {
        Log.d("sizeSelected", "" + it)
        addToCartRequest = AddCartRequest(it.productId, quantity.get()!!)
    }

    fun getCategories(): LiveData<CategoriesResponse> {
        return repo.getCategories()
    }

    fun getType(): LiveData<ManufacturerResponse> {
        return repo.getManufacturer()
    }

    fun getModel(): LiveData<ModelResponse> {
        return repo.getModel()
    }

    fun getProducts(): LiveData<ProductResponse> {
        return repo.getProducts()
    }

    fun progress(): LiveData<Boolean> {
        return repo.progressDialog()
    }

    fun addToCart(addCartRequest: AddCartRequest): LiveData<AddCartResponse> {
        return repo.addToCard(addCartRequest)
    }

    fun setColorsImageAdapter(datum: List<Color>) {
        colorProductAdapter.dataList = datum
        colorProductAdapter.notifyDataSetChanged()
        Log.d("colorProductAdapter", "" + colorProductAdapter.dataList.size)
    }

    fun setSizeAdapter(datum: List<Size>) {
        sizeAdapter.dataList = datum
        sizeAdapter.notifyDataSetChanged()
        Log.d("sizeList", "" + sizeAdapter.dataList.size)
    }

    fun setProductsAdapter(datum: List<Datum>) {
        productsAdapter.dataList = datum
        productsAdapter.notifyDataSetChanged()
        Log.d("productListSize", "" + productsAdapter.dataList.size)
    }

    override fun onCleared() {
        super.onCleared()
        repo.disposable.dispose()
    }
}