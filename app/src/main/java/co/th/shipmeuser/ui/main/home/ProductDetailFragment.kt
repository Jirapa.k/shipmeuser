package co.th.shipmeuser.ui.main.home

import android.util.Log
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.viewpager.widget.ViewPager
import co.th.shipmeuser.R
import co.th.shipmeuser.base.BaseFragment
import co.th.shipmeuser.databinding.FragmentProductDetailBinding
import co.th.shipmeuser.network.models.products.Datum
import co.th.shipmeuser.utils.PRODUCT_MODEL
import org.koin.androidx.viewmodel.ext.android.getViewModel

class ProductDetailFragment : BaseFragment<FragmentProductDetailBinding, HomeViewModel>() {

    var productModel = Datum()

    override fun getLayout(): Int = R.layout.fragment_product_detail
    override fun initViewModel(): HomeViewModel = getViewModel()

    override fun setupView() {
        productModel = arguments?.getParcelable(PRODUCT_MODEL)!!
        binding.viewModel = viewModel
        binding.model = productModel
        imageSliderImplementation()
        onClick()
    }

    private fun onClick() {
        binding.btnAddToCart.setOnClickListener {
            if (viewModel.addToCartRequest.productId.isNotEmpty()) {
                viewModel.addToCart(viewModel.addToCartRequest).observe(this, Observer {
                    Log.d("addToCart", "" + it)
                    Toast.makeText(activity, "เพิ่มสินค้าลงตะกร้าเรียบร้อยแล้ว", Toast.LENGTH_LONG)
                        .show()
                    findNavController().popBackStack()
                })
            } else {
                Toast.makeText(activity, "กรุณาเลือกไซส์ก่อนเพิ่มลงตะกร้า", Toast.LENGTH_LONG)
                    .show()
            }
        }
    }

    private fun imageSliderImplementation() {
        viewModel.setColorsImageAdapter(productModel.color)
        val adapter = ImageSliderAdapter(requireContext(), productModel.color)
        binding.vpProductImage.adapter = adapter
        viewModel.setSizeAdapter(productModel.color[0].size)
        binding.vpProductImage.setOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                Log.d("ImageProductPosition", "" + position)
                viewModel.setSizeAdapter(productModel.color[position].size)
            }
        })
    }

    override fun setObserve() {
    }

}