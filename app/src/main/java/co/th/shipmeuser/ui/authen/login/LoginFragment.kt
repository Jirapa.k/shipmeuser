package co.th.shipmeuser.ui.authen.login

import android.util.Log
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.navigation.fragment.NavHostFragment.findNavController
import co.th.shipmeuser.R
import co.th.shipmeuser.base.BaseFragment
import co.th.shipmeuser.databinding.FragmentLoginBinding
import co.th.shipmeuser.network.models.login.LoginRequest
import co.th.shipmeuser.ui.authen.AuthenViewModel
import co.th.shipmeuser.utils.REST_TOKEN
import co.th.shipmeuser.utils.TOKEN
import co.th.shipmeuser.utils.TOKEN_TYPE
import org.koin.androidx.viewmodel.ext.android.getViewModel

class LoginFragment : BaseFragment<FragmentLoginBinding, AuthenViewModel>() {

    override fun getLayout(): Int = R.layout.fragment_login
    override fun initViewModel(): AuthenViewModel = getViewModel()
    override fun setupView() {
        binding.viewModel = viewModel
        binding.tvBtnSignUp.setOnClickListener {
            val bundle = bundleOf("amount" to "pleum")
            findNavController(this).navigate(R.id.action_loginFragment_to_registerFragment, bundle)
        }

        binding.btnGuestLogin.setOnClickListener {
            findNavController(this).navigate(R.id.action_loginFragment_to_mainActivity)
        }
    }

    override fun setObserve() {
//        viewModel.progress().observe(this, Observer {
//            Log.d("progress", "" + it)
//            when (it) {
//                true -> showProgress()
//                false -> hideProgress()
//            }
//        })

        viewModel.getToken().observe(this, Observer {
            Log.d("token", "" + it)
            sharedPreference.save(TOKEN, it.data?.accessToken!!)
            sharedPreference.save(TOKEN_TYPE, it.data?.tokenType!!)
        })

        binding.btnLogin.setOnClickListener {
            val loginRequest =
                LoginRequest(viewModel.emailLogin.get()!!, viewModel.passwordLogin.get()!!)
            viewModel.login(loginRequest).observe(this, Observer {
                Log.d("LoginResponse", "" + it)
                sharedPreference.save(REST_TOKEN, it.data.restToken)
                findNavController(this).navigate(R.id.action_loginFragment_to_mainActivity)
            })
        }
    }
}
