package co.th.shipmeuser.ui.authen.register

import android.util.Log
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import co.th.shipmeuser.R
import co.th.shipmeuser.base.BaseFragment
import co.th.shipmeuser.databinding.FragmentRegisterBinding
import co.th.shipmeuser.network.models.register.RegisterRequest
import co.th.shipmeuser.ui.authen.AuthenViewModel
import org.koin.androidx.viewmodel.ext.android.getViewModel

class RegisterFragment : BaseFragment<FragmentRegisterBinding, AuthenViewModel>() {

    override fun getLayout(): Int = R.layout.fragment_register
    override fun initViewModel(): AuthenViewModel = getViewModel()
    override fun setupView() {
        binding.viewModel = viewModel
        val text = arguments?.getString("amount")
        Log.d("amount", text)
    }

    override fun setObserve() {
        binding.btnRegister.setOnClickListener {
            val registerRequest = RegisterRequest(
                viewModel.userName.get()!!,
                viewModel.firstName.get()!!,
                viewModel.lastName.get()!!,
                viewModel.email.get()!!,
                viewModel.password.get()!!,
                viewModel.phone.get()!!
            )

            viewModel.register(registerRequest).observe(this, Observer {
                Log.d("registerResponse", "" + it)
                findNavController().popBackStack()
            })
        }

//        viewModel.progress().observe(this, Observer {
//            Log.d("progress", "" + it)
//            when (it) {
//                true -> showProgress()
//                false -> hideProgress()
//            }
//        })
    }
}
