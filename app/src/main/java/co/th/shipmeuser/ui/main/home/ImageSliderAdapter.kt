package co.th.shipmeuser.ui.main.home

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import co.th.shipmeuser.R
import co.th.shipmeuser.databinding.ImageSlideItemBinding
import co.th.shipmeuser.network.models.products.Color

class ImageSliderAdapter(private val context: Context, private val color: List<Color>) :
    PagerAdapter() {

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun getCount(): Int {
        return color.size
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val layoutInflater = LayoutInflater.from(context)
        val binding: ImageSlideItemBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.image_slide_item, container, false)
        binding.model = color[position]
        val vp = container as ViewPager
        vp.addView(binding.root, 0)
        return binding.root
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        val vp = container as ViewPager
        val view = `object` as View
        vp.removeView(view)
    }

}