package co.th.shipmeuser.ui.main.profile

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import co.th.shipmeuser.network.models.logout.LogoutResponse
import co.th.shipmeuser.network.models.profile.ProfileResponse
import co.th.shipmeuser.network.repository.ProfileRepository

class ProfileViewModel(private val repo: ProfileRepository) : ViewModel() {

    fun logout(): LiveData<LogoutResponse> {
        return repo.logout()
    }

    fun getAccount(): LiveData<ProfileResponse> {
        return repo.getAccount()
    }
}