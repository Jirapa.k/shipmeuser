package co.th.shipmeuser.ui.main.home

import android.util.Log
import android.view.View
import android.widget.AdapterView
import androidx.lifecycle.Observer
import co.th.shipmeuser.R
import co.th.shipmeuser.base.BaseFragment
import co.th.shipmeuser.databinding.FragmentHomeBinding
import org.koin.androidx.viewmodel.ext.android.getViewModel

class HomeFragment : BaseFragment<FragmentHomeBinding, HomeViewModel>() {
    val categories = arrayListOf<String>()
    val type = arrayListOf<String>()
    val models = arrayListOf<String>()

    override fun getLayout(): Int = R.layout.fragment_home

    override fun initViewModel(): HomeViewModel = getViewModel()

    override fun setupView() {
        binding.viewModel = viewModel
        addSpinnerHint()
        onProgress()
    }

    override fun setObserve() {
        getProduct()
        setCategoriesSpinner()
        setTypeSpinner()
        setModelSpinner()
    }

    private fun onProgress() {
        viewModel.progress().observe(this, Observer {
            Log.d("progress", "" + it)
            when (it) {
                true -> showProgress()
                false -> hideProgress()
            }
        })
    }

    private fun addSpinnerHint() {
        categories.add(getString(R.string.spinner_hint))
        type.add(getString(R.string.spinner_hint))
        models.add(getString(R.string.spinner_hint))
    }

    private fun getProduct() {
        viewModel.getProducts().observe(this, Observer {
            Log.d("getProduct", "" + it)
            viewModel.setProductsAdapter(it.data)
        })
    }

    private fun setCategoriesSpinner() {
        viewModel.getCategories().observe(this, Observer {
            val list = ArrayList<String>()
            it.data.map { item ->
                list.add(item.name)
            }
            viewModel.categoriesList.value = list
            Log.d("categories", "" + it)
        })

        binding.spBrand.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                Log.d("categories", "" + parent!!.getItemAtPosition(position))
            }
        }
    }

    private fun setTypeSpinner() {
        viewModel.getType().observe(this, Observer {
            val list = ArrayList<String>()
            it.data.map { item ->
                list.add(item.name)
            }
            viewModel.manufacturersList.value = list
            Log.d("types", "" + it)
        })

        binding.spType.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                Log.d("types", "" + parent!!.getItemAtPosition(position))
            }
        }
    }

    private fun setModelSpinner() {
        viewModel.getModel().observe(this, Observer {
            val list = ArrayList<String>()
            it.data.map { item ->
                list.add(item.name)
            }
            viewModel.modelList.value = list
            Log.d("models", "" + it)
        })

        binding.spModel.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                Log.d("models", "" + parent!!.getItemAtPosition(position))
            }
        }
    }
}