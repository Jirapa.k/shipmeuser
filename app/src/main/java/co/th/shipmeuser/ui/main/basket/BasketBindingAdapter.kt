package co.th.shipmeuser.ui.main.basket

import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

object BasketBindingAdapter {

    @BindingAdapter("setCartAdapter")
    @JvmStatic
    fun RecyclerView.bindCartAdapter(adapter: CartAdapter) {
        val dividerItemDecoration = DividerItemDecoration(context, LinearLayoutManager.VERTICAL)
        layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        addItemDecoration(dividerItemDecoration)
        this.adapter = adapter
    }

    @BindingAdapter("setSpinnerAdapter")
    @JvmStatic
    fun Spinner.bindAdapter(adapter: ArrayAdapter<String>) {
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        this.adapter = adapter
        this.setSelection(0)
    }

    @BindingAdapter("setPreviewAdapter")
    @JvmStatic
    fun RecyclerView.bindCartAdapter(adapter: ProductPreviewAdapter) {
        val dividerItemDecoration = DividerItemDecoration(context, LinearLayoutManager.VERTICAL)
        layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        addItemDecoration(dividerItemDecoration)
        this.adapter = adapter
    }
}