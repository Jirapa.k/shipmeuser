package co.th.shipmeuser.ui.main.basket

import android.util.Log
import android.widget.ArrayAdapter
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import co.th.shipmeuser.network.models.AddDataSuccess
import co.th.shipmeuser.network.models.cart.address.AddressRequest
import co.th.shipmeuser.network.models.cart.comfirm.ConfirmResponse
import co.th.shipmeuser.network.models.cart.comfirm.Products
import co.th.shipmeuser.network.models.cart.delete.DeleteCartRequest
import co.th.shipmeuser.network.models.cart.delete.DeleteCartResponse
import co.th.shipmeuser.network.models.cart.getCart.GetCartResponse
import co.th.shipmeuser.network.models.cart.getCart.Product
import co.th.shipmeuser.network.models.cart.payment.PaymentRequest
import co.th.shipmeuser.network.models.cart.searchSeller.GetSellerOrderRequest
import co.th.shipmeuser.network.models.cart.searchSeller.GetSellerResponse
import co.th.shipmeuser.network.models.cart.shipping.AddShippingRequest
import co.th.shipmeuser.network.models.spinner.CountriesRequest
import co.th.shipmeuser.network.models.spinner.CountriesResponse
import co.th.shipmeuser.network.models.spinner.DistrictResponse
import co.th.shipmeuser.network.models.spinner.SubDistrictResponse
import co.th.shipmeuser.network.repository.BasketRepository

class BasketViewModel(private val repo: BasketRepository) : ViewModel() {

    var provinceModel = CountriesResponse()
    var districtModel = DistrictResponse()
    var subDistrictModel = SubDistrictResponse()
    var countriesRequest = CountriesRequest()
    lateinit var provinceAdapter: ArrayAdapter<String>
    lateinit var districtAdapter: ArrayAdapter<String>
    lateinit var subDistrictAdapter: ArrayAdapter<String>
    var postCost: ObservableField<String> = ObservableField("")
    var address: ObservableField<String> = ObservableField("")
    var phone: ObservableField<String> = ObservableField("")
    val onCartDeleted: MutableLiveData<Product> = MutableLiveData()
    var shippingList = arrayListOf("ส่งด่วน", "EMS", "ลงทะเบียน", "KERRY")
    var shippingMethod = arrayListOf("express", "EMS", "R", "kerry")

    val productPreviewAdapter = ProductPreviewAdapter()
    val cartAdapter = CartAdapter {
        onCartDeleted.value = it
    }

    fun setCartAdapter(datum: List<Product>) {
        cartAdapter.dataList = datum as ArrayList<Product>
        cartAdapter.notifyDataSetChanged()
        Log.d("setCartAdapter", "" + cartAdapter.dataList.size)
    }

    fun setProductPreviewAdapter(datum: List<Products>) {
        productPreviewAdapter.dataList = datum
        productPreviewAdapter.notifyDataSetChanged()
        Log.d("productPreview", "" + productPreviewAdapter.dataList.size)
    }

    fun getCart(): LiveData<GetCartResponse> {
        return repo.getCart()
    }

    fun deleteCart(deleteCartRequest: DeleteCartRequest): LiveData<DeleteCartResponse> {
        return repo.deletCart(deleteCartRequest)
    }

    fun getCountries(): LiveData<CountriesResponse> {
        return repo.getCountries(countriesRequest)
    }

    fun getDistrict(districtID: String): LiveData<DistrictResponse> {
        countriesRequest = CountriesRequest("", districtID, "")
        return repo.getDistrict(countriesRequest)
    }

    fun getSubDistrict(subDistrictID: String): LiveData<SubDistrictResponse> {
        countriesRequest = CountriesRequest("", "", subDistrictID)
        return repo.getSubDistrict(countriesRequest)
    }

    fun addAddress(addAddressRequest: AddressRequest): LiveData<AddDataSuccess> {
        return repo.address(addAddressRequest)
    }

    fun addShippingMethod(addShippingRequest: AddShippingRequest): LiveData<AddDataSuccess> {
        return repo.addShippingMethod(addShippingRequest)
    }

    fun addPaymentMethod(paymentRequest: PaymentRequest): LiveData<AddDataSuccess> {
        return repo.addPaymentMethod(paymentRequest)
    }

    fun confirmOrder(): LiveData<ConfirmResponse> {
        return repo.confirmOrder()
    }

    fun getSellerOrder(getSellerOrderRequest: GetSellerOrderRequest): LiveData<GetSellerResponse> {
        return repo.getSellerOrder(getSellerOrderRequest)
    }

    fun checkIndex(index: Int): Boolean = index > -1
}