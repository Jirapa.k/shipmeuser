package co.th.shipmeuser.ui.main.basket

import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import co.th.shipmeuser.R
import co.th.shipmeuser.base.BaseFragment
import co.th.shipmeuser.databinding.FragmentTransferBinding
import co.th.shipmeuser.network.models.cart.shipping.AddShippingRequest
import org.koin.androidx.viewmodel.ext.android.getViewModel

class ShippingFragment : BaseFragment<FragmentTransferBinding, BasketViewModel>() {
    var code = ""
    override fun getLayout(): Int = R.layout.fragment_transfer
    override fun initViewModel(): BasketViewModel = getViewModel()

    override fun setupView() {
        binding.viewModel = viewModel
        onClick()
    }

    override fun setObserve() {

    }

    private fun onClick() {
        binding.spShipping.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                Log.d("shippingSelected", "" + parent!!.getItemAtPosition(position))
                code = viewModel.shippingMethod[position]
            }
        }
        binding.btnAddShipping.setOnClickListener {
            val addShippingRequest = AddShippingRequest("flat.flat", code)
            viewModel.addShippingMethod(addShippingRequest).observe(this, Observer {
                Log.d("addShippingMethod", "" + it)
                Toast.makeText(activity, "เพิ่มช่องทางการจัดส่งเรียบร้อยแล้ว", Toast.LENGTH_LONG)
                    .show()
                findNavController().navigate(R.id.action_shippingFragment_to_paymentFragment)
            })
        }
        binding.btnBack.setOnClickListener {
            findNavController().popBackStack()
        }
    }
}
