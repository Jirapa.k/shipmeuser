package co.th.shipmeuser.ui.main.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import co.th.shipmeuser.R
import co.th.shipmeuser.databinding.ColorItemBinding
import co.th.shipmeuser.databinding.ColorProductsItemBinding
import co.th.shipmeuser.network.models.products.Color

class ColorsImageAdapter(val onClick: (Color) -> Unit) :
    RecyclerView.Adapter<ColorsImageAdapter.ViewHolder>() {

    var dataList: List<Color> = emptyList()

    override fun getItemCount(): Int = dataList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: ColorProductsItemBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.color_products_item, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(dataList[position])
        holder.itemView.setOnClickListener { onClick(dataList[position]) }
    }

    class ViewHolder(private val binding: ColorProductsItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindItem(data: Color) {
            binding.model = data
        }
    }
}