package co.th.shipmeuser.ui.main.basket

import android.util.Log
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import co.th.shipmeuser.R
import co.th.shipmeuser.base.BaseFragment
import co.th.shipmeuser.databinding.FragmentBasketBinding
import co.th.shipmeuser.network.models.cart.delete.DeleteCartRequest
import org.koin.androidx.viewmodel.ext.android.getViewModel

class BasketFragment : BaseFragment<FragmentBasketBinding, BasketViewModel>() {

    override fun getLayout(): Int = R.layout.fragment_basket
    override fun initViewModel(): BasketViewModel = getViewModel()

    override fun setupView() {
        binding.viewModel = viewModel
        onClick()
    }

    override fun setObserve() {
        viewModel.getCart().observe(this, Observer {
            Log.d("getCartResponse", "" + it)
            viewModel.setCartAdapter(it.data.products)
        })

        viewModel.onCartDeleted.observe(this, Observer { product ->
            Log.d("OnDeleteCart", "" + product)
            val deleteCartRequest = DeleteCartRequest(product.key)
            viewModel.deleteCart(deleteCartRequest).observe(this, Observer {
                Log.d("DeletedCart", "" + it)
                viewModel.cartAdapter.delete(product)
                Toast.makeText(activity, "สบรายการสินค้าเรียบร้อยแล้ว", Toast.LENGTH_LONG).show()
            })
        })
    }

    private fun onClick() {
        binding.btnOrder.setOnClickListener {
            findNavController().navigate(R.id.action_navigation_Basket_to_addressFragment)
        }
    }
}
