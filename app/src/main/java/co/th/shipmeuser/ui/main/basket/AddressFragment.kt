package co.th.shipmeuser.ui.main.basket

import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import co.th.shipmeuser.R
import co.th.shipmeuser.base.BaseFragment
import co.th.shipmeuser.databinding.FragmentAddressBinding
import co.th.shipmeuser.network.models.cart.address.AddressRequest
import org.koin.androidx.viewmodel.ext.android.getViewModel

class AddressFragment : BaseFragment<FragmentAddressBinding, BasketViewModel>() {

    val province = arrayListOf<String>()
    val district = arrayListOf<String>()
    val subDistrict = arrayListOf<String>()
    var provinceId = ""
    var districtId = ""
    var subDistrictId = ""

    override fun getLayout(): Int = R.layout.fragment_address
    override fun initViewModel(): BasketViewModel = getViewModel()

    override fun setupView() {
        binding.viewModel = viewModel
        onClick()
    }

    override fun setObserve() {
        province.add("จังหวัด")
        district.add("เขต/อำเภอ")
        subDistrict.add("แขวง/ตำบล")

        viewModel.getCountries().observe(this, Observer {
            Log.d("getProvince", "" + it)
            viewModel.provinceModel = it
            it.data.forEach { data ->
                province.add(data.name!!)
            }
        })
        viewModel.provinceAdapter =
            ArrayAdapter(requireContext(), android.R.layout.simple_spinner_dropdown_item, province)

        binding.spProvince.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                Log.d("onProvinceSelected", "" + parent!!.getItemAtPosition(position))
                binding.spDistrict.setSelection(0)
                if (viewModel.checkIndex(position - 1)) {
                    provinceId = viewModel.provinceModel.data[position - 1].zoneId!!
                    viewModel.getDistrict(viewModel.provinceModel.data[position - 1].zoneId!!)
                        .observe(this@AddressFragment, Observer {
                            Log.d("getDistrict", "" + it)
                            viewModel.districtModel = it
                            district.clear()
                            district.add("เขต/อำเภอ")
                            it.data.forEach { data ->
                                district.add(data.name)
                            }
                        })
                }

            }
        }
        viewModel.districtAdapter =
            ArrayAdapter(requireContext(), android.R.layout.simple_spinner_dropdown_item, district)

        binding.spDistrict.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                Log.d("onDistrictSelected", "" + parent!!.getItemAtPosition(position))
                binding.spSubDistrict.setSelection(0)
                if (viewModel.checkIndex(position - 1)) {
                    districtId = viewModel.districtModel.data[position - 1].districtId
                    viewModel.getSubDistrict(viewModel.districtModel.data[position - 1].districtId)
                        .observe(this@AddressFragment, Observer {
                            Log.d("getSubDistrict", "" + it)
                            viewModel.subDistrictModel = it
                            subDistrict.clear()
                            subDistrict.add("แขวง/ตำบล")
                            it.data.forEach { data ->
                                subDistrict.add(data.name)
                            }
                        })
                }

            }
        }
        viewModel.subDistrictAdapter = ArrayAdapter(
            requireContext(),
            android.R.layout.simple_spinner_dropdown_item,
            subDistrict
        )

        binding.spSubDistrict.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                Log.d("onSubDistrictSelected", "" + parent!!.getItemAtPosition(position))
                // viewModel.postCost.set("")
                if (viewModel.checkIndex(position - 1)) {
                    subDistrictId = viewModel.subDistrictModel.data[position - 1].subdistrictId
                    viewModel.postCost.set(viewModel.subDistrictModel.data[position - 1].zipCode)
                }
            }
        }
    }

    private fun onClick() {
        binding.btnCancelAdd.setOnClickListener {
            findNavController().popBackStack()
        }

        binding.btnAddAddress.setOnClickListener {
            val addAddressRequest =
                AddressRequest(
                    viewModel.address.get()!!,
                    provinceId,
                    districtId,
                    subDistrictId,
                    viewModel.postCost.get()!!
                )
            Log.d("addressModel", "" + addAddressRequest)
            viewModel.addAddress(addAddressRequest).observe(this, Observer {
                Log.d("addAddress", "" + it)
                Toast.makeText(activity, "เพิ่มที่อยู่การจัดส่งเรียร้อยแล้ว", Toast.LENGTH_LONG)
                    .show()
                findNavController().navigate(R.id.action_addressFragment_to_shippingFragment)
            })
        }
    }
}
