package co.th.shipmeuser.ui.main.basket

import android.util.Log
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import co.th.shipmeuser.R
import co.th.shipmeuser.base.BaseFragment
import co.th.shipmeuser.databinding.FragmentCheckOrderInfoBinding
import co.th.shipmeuser.utils.ORDER_ID
import org.koin.androidx.viewmodel.ext.android.getViewModel

class PreviewOrderFragment : BaseFragment<FragmentCheckOrderInfoBinding, BasketViewModel>() {

    var orderID = ""
    override fun getLayout(): Int = R.layout.fragment_check_order_info
    override fun initViewModel(): BasketViewModel = getViewModel()

    override fun setupView() {
        binding.viewModel = viewModel
        onClick()
    }

    override fun setObserve() {
        viewModel.confirmOrder().observe(this, Observer {
            Log.d("confirmOrder", "" + it)
            orderID = it.data.orderId.toString()
            binding.model = it.data
            viewModel.setProductPreviewAdapter(it.data.products)
        })
    }

    private fun onClick() {
        binding.btnBack.setOnClickListener {
            findNavController().popBackStack()
        }

        binding.btnSearchStore.setOnClickListener {
            val bundle = bundleOf(ORDER_ID to orderID)
            findNavController().navigate(
                R.id.action_previewOrderFragment_to_searchOrderFragment,
                bundle
            )
        }
    }
}
