package co.th.shipmeuser.ui.main.basket

import android.util.Log
import android.widget.RadioGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import co.th.shipmeuser.R
import co.th.shipmeuser.base.BaseFragment
import co.th.shipmeuser.databinding.FragmentPaymentBinding
import co.th.shipmeuser.network.models.cart.payment.PaymentRequest
import co.th.shipmeuser.utils.BANKING
import co.th.shipmeuser.utils.CASH_ON_DELIVERY
import co.th.shipmeuser.utils.CREDIT_CARD
import org.koin.androidx.viewmodel.ext.android.getViewModel

class PaymentFragment : BaseFragment<FragmentPaymentBinding, BasketViewModel>() {
    var paymentMethod = ""
    override fun getLayout(): Int = R.layout.fragment_payment
    override fun initViewModel(): BasketViewModel = getViewModel()

    override fun setupView() {
        binding.viewModel = viewModel
        onClick()
    }

    override fun setObserve() {

    }

    private fun onClick() {
        binding.rbGroup.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.rb_cash_on_delivery -> {
                    paymentMethod = CASH_ON_DELIVERY
                }
                R.id.rb_banking -> {
                    paymentMethod = BANKING
                }
                R.id.rb_credit_card -> {
                    paymentMethod = CREDIT_CARD
                }
            }
        })
        binding.btnAddPayment.setOnClickListener {
            val paymentRequest = PaymentRequest(paymentMethod)
            Log.d("paymentRequest", "" + paymentRequest)
            viewModel.addPaymentMethod(paymentRequest).observe(this, Observer {
                Log.d("addPaymentMethod", "" + it)
                Toast.makeText(activity, "เพิ่มช่องทางการชำระงินเรียบร้อยแล้ว", Toast.LENGTH_LONG)
                    .show()
                findNavController().navigate(R.id.action_paymentFragment_to_previewOrderFragment)
            })
        }
        binding.btnBack.setOnClickListener {
            findNavController().popBackStack()
        }
    }
}
