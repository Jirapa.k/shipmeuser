package co.th.shipmeuser.ui.authen

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import co.th.shipmeuser.network.models.login.LoginRequest
import co.th.shipmeuser.network.models.login.LoginResponse
import co.th.shipmeuser.network.models.register.RegisterRequest
import co.th.shipmeuser.network.models.register.RegisterResponse
import co.th.shipmeuser.network.models.token.TokenResponseModel
import co.th.shipmeuser.network.repository.AuthenRepository

class AuthenViewModel(private val repo: AuthenRepository) : ViewModel() {
    val errorEmail = "Invalid Email"

    var emailLogin: ObservableField<String> = ObservableField("pleum12@apimail.com")
    var passwordLogin: ObservableField<String> = ObservableField("password")

    var firstName: ObservableField<String> = ObservableField("")
    var lastName: ObservableField<String> = ObservableField("")
    var email: ObservableField<String> = ObservableField("")
    var userName: ObservableField<String> = ObservableField("")
    var password: ObservableField<String> = ObservableField("")
    var phone: ObservableField<String> = ObservableField("")

    fun getToken(): LiveData<TokenResponseModel> {
        return repo.getToken()
    }

    fun register(registerRequest: RegisterRequest): LiveData<RegisterResponse> {
        return repo.register(registerRequest)
    }

    fun progress(): LiveData<Boolean> {
        return repo.progressDialog()
    }

    fun login(loginRequest: LoginRequest): LiveData<LoginResponse> {
        return repo.login(loginRequest)
    }

    fun disposeService(){
        repo.disposeService()
    }

    override fun onCleared() {
        super.onCleared()
        repo.disposeService()
    }
}