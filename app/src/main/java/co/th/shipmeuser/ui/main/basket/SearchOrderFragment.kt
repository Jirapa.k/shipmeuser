package co.th.shipmeuser.ui.main.basket

import android.util.Log
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import co.th.shipmeuser.R
import co.th.shipmeuser.base.BaseFragment
import co.th.shipmeuser.databinding.FragmentSearchOrderBinding
import co.th.shipmeuser.network.models.cart.searchSeller.GetSellerOrderRequest
import co.th.shipmeuser.utils.ORDER_ID
import org.koin.androidx.viewmodel.ext.android.getViewModel

class SearchOrderFragment : BaseFragment<FragmentSearchOrderBinding, BasketViewModel>() {
    var orderID = ""
    override fun getLayout(): Int = R.layout.fragment_search_order
    override fun initViewModel(): BasketViewModel = getViewModel()

    override fun setupView() {
        orderID = arguments?.getString(ORDER_ID)!!
        binding.viewModel = viewModel
        onClick()
    }

    override fun setObserve() {
        viewModel.getSellerOrder(GetSellerOrderRequest(orderID)).observe(this, Observer {
            Log.d("getSellerOrder", "" + it)
        })
    }

    private fun onClick() {
        binding.btnCancel.setOnClickListener {
            findNavController().popBackStack()
        }
    }
}
