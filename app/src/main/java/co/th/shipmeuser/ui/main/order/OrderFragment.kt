package co.th.shipmeuser.ui.main.order

import co.th.shipmeuser.R
import co.th.shipmeuser.base.BaseFragment
import co.th.shipmeuser.databinding.FragmentOrderBinding
import org.koin.androidx.viewmodel.ext.android.getViewModel

class OrderFragment : BaseFragment<FragmentOrderBinding, OrderViewModel>() {
    override fun getLayout(): Int = R.layout.fragment_order

    override fun initViewModel(): OrderViewModel = getViewModel()

    override fun setupView() {
        binding.viewModel = viewModel
    }

    override fun setObserve() {
//        viewModel.getOrders().observe(this, Observer {
//            Log.d("orderLists", "" + it)
//        })
    }
}