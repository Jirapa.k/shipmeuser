package co.th.shipmeuser.ui.main.profile

import android.util.Log
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import co.th.shipmeuser.R
import co.th.shipmeuser.base.BaseFragment
import co.th.shipmeuser.databinding.FragmentProfileBinding
import org.koin.androidx.viewmodel.ext.android.getViewModel

class ProfileFragment : BaseFragment<FragmentProfileBinding, ProfileViewModel>() {
    override fun getLayout(): Int = R.layout.fragment_profile

    override fun initViewModel(): ProfileViewModel = getViewModel()

    override fun setupView() {
        binding.viewModel = viewModel
    }

    override fun setObserve() {
        viewModel.getAccount().observe(this, Observer {
            Log.d("profileResponse", "" + it)
            binding.model = it.data
        })

        binding.btnLogout.setOnClickListener {
            viewModel.logout().observe(this, Observer {
                Log.d("logoutResponse", "" + it)
                Toast.makeText(activity, "ออกจากระบบเรียบร้อยแล้ว", Toast.LENGTH_LONG).show()
                findNavController().navigate(R.id.action_navigation_profile_to_authenActivity)
            })
        }
    }

}