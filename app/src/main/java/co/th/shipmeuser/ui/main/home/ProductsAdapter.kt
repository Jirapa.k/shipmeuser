package co.th.shipmeuser.ui.main.home

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import co.th.shipmeuser.R
import co.th.shipmeuser.databinding.ProductItemBinding
import co.th.shipmeuser.network.models.products.Color
import co.th.shipmeuser.network.models.products.Datum
import co.th.shipmeuser.utils.PRODUCT_MODEL

class ProductsAdapter :
    RecyclerView.Adapter<ProductsAdapter.ViewHolder>() {

    var dataList: List<Datum> = emptyList()

    override fun getItemCount(): Int = dataList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: ProductItemBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.product_item, parent, false)
        return ViewHolder(binding, parent.context)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(dataList[position])
        holder.itemView.setOnClickListener {
            val bundle = bundleOf(PRODUCT_MODEL to dataList[position])
            it.findNavController()
                .navigate(R.id.action_navigation_home_to_productDetailFragment, bundle)
        }
    }

    class ViewHolder(private val binding: ProductItemBinding, private val context: Context) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindItem(data: Datum) {
            binding.model = data
            setColorAdapter(binding.rvColor, context, data.color)
        }

        private fun setColorAdapter(
            recyclerView: RecyclerView,
            context: Context,
            color: List<Color>
        ) {
            val colorsAdapter = ColorsAdapter()
            colorsAdapter.dataList = color
            recyclerView.layoutManager =
                LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            recyclerView.adapter = colorsAdapter
        }
    }
}