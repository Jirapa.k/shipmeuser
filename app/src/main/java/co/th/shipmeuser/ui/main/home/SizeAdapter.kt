package co.th.shipmeuser.ui.main.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import co.th.shipmeuser.R
import co.th.shipmeuser.databinding.SizeItemBinding
import co.th.shipmeuser.network.models.products.Size

class SizeAdapter(val onClick: (Size) -> Unit) :
    RecyclerView.Adapter<SizeAdapter.ViewHolder>() {

    var dataList: List<Size> = emptyList()

    override fun getItemCount(): Int = dataList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: SizeItemBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.size_item, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(dataList[position])
        holder.itemView.setOnClickListener { }
        holder.itemView.setOnClickListener { onClick(dataList[position]) }
    }

    class ViewHolder(private val binding: SizeItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindItem(data: Size) {
            binding.model = data
        }
    }
}