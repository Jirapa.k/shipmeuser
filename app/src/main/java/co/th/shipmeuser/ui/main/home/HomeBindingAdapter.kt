package co.th.shipmeuser.ui.main.home

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

object HomeBindingAdapter {

    @BindingAdapter("setProductsAdapter")
    @JvmStatic
    fun RecyclerView.bindProductsAdapter(adapter: ProductsAdapter) {
        layoutManager = GridLayoutManager(context, 2)
        this.adapter = adapter
    }

    @BindingAdapter("setSizeAdapter")
    @JvmStatic
    fun RecyclerView.bindSizeAdapter(adapter: SizeAdapter) {
        layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        this.adapter = adapter
    }

    @BindingAdapter("setColorAdapter")
    @JvmStatic
    fun RecyclerView.bindColorsAdapter(adapter: ColorsImageAdapter) {
        layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        this.adapter = adapter
    }
}