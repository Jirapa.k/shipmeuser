package co.th.shipmeuser.base

import android.content.IntentFilter
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModel
import co.th.shipmeuser.R
import co.th.shipmeuser.utils.ConnectivityReceiver

//import th.co.aware.common.helper.LocaleHelper

abstract class BaseActivity<Binding : ViewDataBinding, Model : ViewModel> : AppCompatActivity(),
    ConnectivityReceiver.ConnectivityReceiverListener {

    lateinit var binding: Binding
    lateinit var viewModel: Model
    lateinit var alertDialog: AlertDialog

//    override fun attachBaseContext(base: Context) {
//        super.attachBaseContext(LocaleHelper.onAttach(base, LocaleHelper.getLanguage(base)))
//    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, getLayout())
        binding.lifecycleOwner = this
        viewModel = initViewModel()
        alertDialog = AlertDialog.Builder(this).create()
        alertDialog.setView(View.inflate(this, R.layout.loading_dialog, null))
        alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
        //alertDialog.setMessage(message)
        alertDialog.setCancelable(false)
        registerReceiver(
            ConnectivityReceiver(),
            IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        )
        setFragment()
        setupView()
        setObserve()
    }

    abstract fun getLayout(): Int
    abstract fun initViewModel(): Model
    abstract fun setupView()
    abstract fun setObserve()
    abstract fun setFragment()

    override fun onResume() {
        super.onResume()
        ConnectivityReceiver.connectivityReceiverListener = this
    }

    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        Toast.makeText(this,isConnected.toString(),Toast.LENGTH_LONG).show()
    }

    fun showProgress() {
        if(alertDialog != null && !alertDialog.isShowing())
            alertDialog.show();
    }

    fun hideProgress() {
        if (alertDialog.isShowing && alertDialog != null) {
            alertDialog.dismiss()
        }
    }
}
