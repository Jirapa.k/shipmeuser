package co.th.shipmeuser.base

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import co.th.shipmeuser.R
import co.th.shipmeuser.network.pref.pref.SharedPreference

abstract class BaseFragment<Binding : ViewDataBinding, Model : ViewModel> : Fragment() {

    lateinit var viewModel: Model
    lateinit var binding: Binding
    lateinit var sharedPreference: SharedPreference
    lateinit var alertDialog: AlertDialog

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, getLayout(), container, false)
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        sharedPreference = SharedPreference(activity!!)
        alertDialog = AlertDialog.Builder(activity!!).create()
        alertDialog.setView(View.inflate(context, R.layout.loading_dialog, null))
        alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
        //alertDialog.setMessage(message)
        alertDialog.setCancelable(false)
        activity?.let {
            viewModel = initViewModel()
        }
        setupView()
        setObserve()
    }

    abstract fun getLayout(): Int
    abstract fun initViewModel(): Model
    abstract fun setupView()
    abstract fun setObserve()

    fun showProgress() {
        if(alertDialog != null && !alertDialog.isShowing())
            alertDialog.show();
    }

    fun hideProgress() {
        if (alertDialog.isShowing && alertDialog != null) {
            alertDialog.dismiss()
        }
    }
}
