package com.example.jirapakannasut.awareandroidtest.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "student")
data class StudentEntity(
    @PrimaryKey(autoGenerate = true) var id: Int = 0,
    @ColumnInfo(name = "student_code") var code: Long = 0,
    @ColumnInfo(name = "first_name") var firstName: String = "",
    @ColumnInfo(name = "last_name") var lastName: String = "",
    var email: String = "",
    var gender: String = "",
    var address: String = ""
    //@Embedded var address: AddressModel? = null)
)