package co.th.shipmeuser.network.models.spinner

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class CountriesRequest(
    @SerializedName("country_id")
    @Expose var countryId: String = "209",

    @SerializedName("zone_id")
    @Expose var zoneId: String = "",

    @SerializedName("district_id")
    @Expose var districtId: String = ""
)