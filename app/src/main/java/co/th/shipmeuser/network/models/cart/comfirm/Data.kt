package co.th.shipmeuser.network.models.cart.comfirm

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Data(
    @SerializedName("totals")
    @Expose
    var totals: List<Total> = arrayListOf(),

    @SerializedName("invoice_prefix")
    @Expose
    var invoicePrefix: String = "",

    @SerializedName("store_id")
    @Expose
    var storeId: Int = 0,

    @SerializedName("store_name")
    @Expose
    var storeName: String = "",

    @SerializedName("store_url")
    @Expose
    var storeUrl: String = "",

    @SerializedName("customer_id")
    @Expose
    var customerId: String = "",

    @SerializedName("customer_group_id")
    @Expose
    var customerGroupId: String = "",

    @SerializedName("firstname")
    @Expose
    var firstname: String = "",

    @SerializedName("lastname")
    @Expose
    var lastname: String = "",

    @SerializedName("email")
    @Expose
    var email: String = "",

    @SerializedName("telephone")
    @Expose
    var telephone: String = "",

    @SerializedName("fax")
    @Expose
    var fax: String = "",

    @SerializedName("custom_field")
    @Expose
    var customField: String = "",

    @SerializedName("payment_firstname")
    @Expose
    var paymentFirstname: String = "",

    @SerializedName("payment_lastname")
    @Expose
    var paymentLastname: String = "",

    @SerializedName("payment_company")
    @Expose
    var paymentCompany: String = "",

    @SerializedName("payment_address_1")
    @Expose
    var paymentAddress1: String = "",

    @SerializedName("payment_address_2")
    @Expose
    var paymentAddress2: String = "",

    @SerializedName("payment_city")
    @Expose
    var paymentCity: String = "",

    @SerializedName("payment_postcode")
    @Expose
    var paymentPostcode: String = "",

    @SerializedName("payment_zone")
    @Expose

    var paymentZone: String = "",

    @SerializedName("payment_zone_id")
    @Expose
    var paymentZoneId: String = "",

    @SerializedName("payment_country")
    @Expose
    var paymentCountry: String = "",

    @SerializedName("payment_country_id")
    @Expose
    var paymentCountryId: String = "",

    @SerializedName("payment_address_format")
    @Expose
    var paymentAddressFormat: String = "",

    @SerializedName("payment_custom_field")
    @Expose
    var paymentCustomField: String = "",

    @SerializedName("payment_method")
    @Expose
    var paymentMethod: String = "",

    @SerializedName("payment_code")
    @Expose
    var paymentCode: String = "",

    @SerializedName("shipping_firstname")
    @Expose
    var shippingFirstname: String = "",

    @SerializedName("shipping_lastname")
    @Expose
    var shippingLastname: String = "",

    @SerializedName("shipping_company")
    @Expose
    var shippingCompany: String = "",

    @SerializedName("shipping_address_1")
    @Expose
    var shippingAddress1: String = "",

    @SerializedName("shipping_address_2")
    @Expose
    var shippingAddress2: String = "",

    @SerializedName("shipping_city")
    @Expose
    var shippingCity: String = "",

    @SerializedName("shipping_postcode")
    @Expose
    var shippingPostcode: String = "",

    @SerializedName("shipping_zone")
    @Expose
    var shippingZone: String = "",

    @SerializedName("shipping_zone_id")
    @Expose
    var shippingZoneId: String = "",

    @SerializedName("shipping_country")
    @Expose
    var shippingCountry: String = "",

    @SerializedName("shipping_country_id")
    @Expose
    var shippingCountryId: String = "",

    @SerializedName("shipping_address_format")
    @Expose
    var shippingAddressFormat: String = "",

    @SerializedName("shipping_custom_field")
    @Expose
    var shippingCustomField: List<String> = arrayListOf(),

    @SerializedName("shipping_method")
    @Expose
    var shippingMethod: String = "",

    @SerializedName("shipping_code")
    @Expose
    var shippingCode: String = "",

    @SerializedName("products")
    @Expose
    var products: List<Products> = arrayListOf(),

    @SerializedName("vouchers")
    @Expose
    var vouchers: List<String> = arrayListOf(),

    @SerializedName("shipping_detail")
    @Expose
    var shippingDetail: String = "",

    @SerializedName("total")
    @Expose
    var total: Int = 0,

    @SerializedName("affiliate_id")
    @Expose
    var affiliateId: Int = 0,

    @SerializedName("commission")
    @Expose
    var commission: Int = 0,

    @SerializedName("marketing_id")
    @Expose
    var marketingId: Int = 0,

    @SerializedName("tracking")
    @Expose
    var tracking: String = "",

    @SerializedName("language_id")
    @Expose
    var languageId: String = "",

    @SerializedName("currency_id")
    @Expose
    var currencyId: String = "",

    @SerializedName("currency_code")
    @Expose
    var currencyCode: String = "",

    @SerializedName("currency_value")
    @Expose
    var currencyValue: String = "",

    @SerializedName("ip")
    @Expose
    var ip: String = "",

    @SerializedName("forwarded_ip")
    @Expose
    var forwardedIp: String = "",

    @SerializedName("user_agent")
    @Expose
    var userAgent: String = "",

    @SerializedName("accept_language")
    @Expose
    var acceptLanguage: String = "",

    @SerializedName("order_id")
    @Expose
    var orderId: Int = 0
)