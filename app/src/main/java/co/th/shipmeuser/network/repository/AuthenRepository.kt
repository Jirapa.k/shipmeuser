package co.th.shipmeuser.network.repository

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import co.th.shipmeuser.network.api.ApiInterface
import co.th.shipmeuser.network.models.login.ErrorResponse
import co.th.shipmeuser.network.models.login.LoginRequest
import co.th.shipmeuser.network.models.login.LoginResponse
import co.th.shipmeuser.network.models.register.RegisterRequest
import co.th.shipmeuser.network.models.register.RegisterResponse
import co.th.shipmeuser.network.models.spinner.CountriesRequest
import co.th.shipmeuser.network.models.spinner.CountriesResponse
import co.th.shipmeuser.network.models.spinner.DistrictResponse
import co.th.shipmeuser.network.models.spinner.SubDistrictResponse
import co.th.shipmeuser.network.models.token.TokenResponseModel
import co.th.shipmeuser.network.pref.pref.SharedPreference
import co.th.shipmeuser.utils.REST_TOKEN
import co.th.shipmeuser.utils.TOKEN
import co.th.shipmeuser.utils.TOKEN_TYPE
import com.google.gson.Gson
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException


class AuthenRepository(
    private val client: ApiInterface,
    private val sharedPreferences: SharedPreference,
    private val context: Context
) {
    private val responseModel = MutableLiveData<TokenResponseModel>()
    private val countriesModel = MutableLiveData<CountriesResponse>()
    private val districtModel = MutableLiveData<DistrictResponse>()
    private val subDistrictModel = MutableLiveData<SubDistrictResponse>()
    private val registerResponse = MutableLiveData<RegisterResponse>()
    private val loginResponse = MutableLiveData<LoginResponse>()
    private val authenToken =
        sharedPreferences.getValueString(TOKEN_TYPE) + " " + sharedPreferences.getValueString(TOKEN)
    private val restToken =
        sharedPreferences.getValueString(REST_TOKEN)
    private val progressbarObservable = MutableLiveData<Boolean>()
    lateinit var disposable: Disposable

    fun getToken(): LiveData<TokenResponseModel> {
        disposable = client.getToken("Basic c2hpcG1lOnNoaXBtZV9zZWNyZXQ=")
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { progressbarObservable.postValue(true) }
            .doOnComplete { progressbarObservable.postValue(false) }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                responseModel.postValue(it)
            }, {
                progressbarObservable.postValue(false)
                it.printStackTrace()
            })
        return responseModel
    }

    fun getCountries(countriesRequest: CountriesRequest): LiveData<CountriesResponse> {
        disposable = client.getProvince(restToken!!, countriesRequest)
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { progressbarObservable.postValue(true) }
            .doOnComplete { progressbarObservable.postValue(false) }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                countriesModel.postValue(it)
            }, {
                progressbarObservable.value = false
                it.printStackTrace()
                Log.e("error", it.localizedMessage!!)
            })
        return countriesModel
    }

    fun login(loginRequest: LoginRequest): LiveData<LoginResponse> {
        disposable = client.login(authenToken, "customer", loginRequest)
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { progressbarObservable.postValue(true) }
            .doOnComplete { progressbarObservable.postValue(false) }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                loginResponse.postValue(it)
            }, {
                progressbarObservable.postValue(false)
                if (it is HttpException) {
                    val errorJsonString = it.response().errorBody()?.string()
                    val response = Gson().fromJson(errorJsonString, ErrorResponse::class.java)
                    // response?.error?.get(0)?.let { it1 -> DialogUtils(context).alertDialog(it1) }
                    Log.e("error", "" + response)
                } else {
                    Log.e("error", it.message)
                }
            })
        return loginResponse
    }

    fun getDistrict(countriesRequest: CountriesRequest): LiveData<DistrictResponse> {
        disposable = client.getDistrict(restToken!!, countriesRequest)
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { progressbarObservable.postValue(true) }
            .doOnComplete { progressbarObservable.postValue(false) }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                progressbarObservable.value = false
                districtModel.postValue(it)
            }, {
                progressbarObservable.value = false
                it.printStackTrace()
                Log.e("error", it.localizedMessage!!)
            })
        return districtModel
    }

    fun getSubDistrict(countriesRequest: CountriesRequest): LiveData<SubDistrictResponse> {
        progressbarObservable.postValue(true)
        disposable = client.getSubDistrict(restToken!!, countriesRequest)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                progressbarObservable.postValue(false)
                subDistrictModel.postValue(it)
            }, {
                progressbarObservable.postValue(false)
                it.printStackTrace()
                Log.e("error", it.localizedMessage!!)
            })
        return subDistrictModel
    }

    fun register(registerRequest: RegisterRequest): LiveData<RegisterResponse> {
        disposable = client.register(restToken!!, registerRequest)
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { progressbarObservable.postValue(true) }
            .doOnComplete { progressbarObservable.postValue(false) }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                registerResponse.postValue(it)
            }, {
                progressbarObservable.postValue(false)
                if (it is HttpException) {
                    val errorJsonString = it.response().errorBody()?.string()
                    val response = Gson().fromJson(errorJsonString, ErrorResponse::class.java)
                    //response?.error?.get(0)?.let { it1 -> DialogUtils(context).alertDialog(it) }
                    Log.e("error", "" + response)
                } else {
                    Log.e("error", it.message)
                }
            })
        return registerResponse
    }

    fun progressDialog(): MutableLiveData<Boolean> {
        return progressbarObservable
    }

    fun disposeService() {
        disposable.dispose()
    }

}
