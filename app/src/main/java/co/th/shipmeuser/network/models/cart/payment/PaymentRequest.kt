package co.th.shipmeuser.network.models.cart.payment

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class PaymentRequest(
    @SerializedName("payment_method")
    @Expose
    var paymentMethod: String = ""
)