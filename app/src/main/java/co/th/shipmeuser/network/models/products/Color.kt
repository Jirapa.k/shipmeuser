package co.th.shipmeuser.network.models.products

import android.os.Parcelable
import co.th.shipmeuser.network.models.products.Size
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Color(
    @SerializedName("name")
    @Expose
    var name: String = "",

    @SerializedName("model")
    @Expose
    var model: String = "",

    @SerializedName("manufacturer_id")
    @Expose
    var manufacturerId: String = "",

    @SerializedName("manufacturer")
    @Expose
    var manufacturer: String = "",

    @SerializedName("category_id")
    @Expose
    var categoryId: String = "",

    @SerializedName("category_name")
    @Expose
    var categoryName: String = "",

    @SerializedName("description")
    @Expose
    var description: String = "",

    @SerializedName("color")
    @Expose
    var color: String = "",

    @SerializedName("price")
    @Expose
    var price: Int = 0,

    @SerializedName("image_color")
    @Expose
    var imageColor: String = "",

    @SerializedName("image_product")
    @Expose
    var imageProduct: String = "",

    @SerializedName("size")
    @Expose
    var size: List<Size> = arrayListOf()
):Parcelable