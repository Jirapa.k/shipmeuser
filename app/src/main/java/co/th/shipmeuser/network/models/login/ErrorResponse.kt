package co.th.shipmeuser.network.models.login

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ErrorResponse(

    @SerializedName("success")
    @Expose var success: Int? = 0,

    @SerializedName("error")
    @Expose var error: List<String>? = arrayListOf(),

    @SerializedName("data")
    @Expose var data: List<String> = arrayListOf()
)