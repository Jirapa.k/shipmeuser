package co.th.shipmeuser.network.repository

import co.th.shipmeuser.network.api.ApiInterface
import co.th.shipmeuser.network.pref.pref.SharedPreference
import co.th.shipmeuser.utils.REST_TOKEN
import co.th.shipmeuser.utils.TOKEN
import co.th.shipmeuser.utils.TOKEN_TYPE
import io.reactivex.disposables.Disposable

class OrderRepository(
    private val client: ApiInterface,
    private val sharedPreferences: SharedPreference
) {
    //private val orderResponse = MutableLiveData<OrderResponse>()

    private val authenToken =
        sharedPreferences.getValueString(REST_TOKEN)

    lateinit var disposable: Disposable

//    fun getOrders(): LiveData<OrderResponse> {
//        disposable = client.getSellerOrder(authenToken)
//            .subscribeOn(Schedulers.io())
//            .observeOn(AndroidSchedulers.mainThread())
//            .subscribe({
//                orderResponse.postValue(it)
//            }, {
//                if (it is HttpException) {
//                    val errorJsonString = it.response().errorBody()?.string()
//                    val response = Gson().fromJson(errorJsonString, ErrorResponse::class.java)
//                    //response?.error?.get(0)?.let { it1 -> DialogUtils(context).alertDialog(it1) }
//                    Log.e("error", "" + response)
//                } else {
//                    Log.e("error", it.message)
//                }
//            })
//        return orderResponse
//    }

}
