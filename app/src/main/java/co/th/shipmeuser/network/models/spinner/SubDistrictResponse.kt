package co.th.shipmeuser.network.models.spinner

import co.th.shipmeuser.network.models.spinner.SubDistrict
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class SubDistrictResponse(
    @SerializedName("success")
    @Expose var success: Int? = 0,

    @SerializedName("error")
    @Expose var error: List<String>? = null,

    @SerializedName("data")
    @Expose var data: List<SubDistrict> = arrayListOf()
)