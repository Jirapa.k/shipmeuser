package co.th.shipmeuser.network.models.login

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Data(
    @SerializedName("customer_id")
    @Expose var customerId: String = "",

    @SerializedName("customer_group_id")
    @Expose var customerGroupId: String = "",

    @SerializedName("store_id")
    @Expose var storeId: String = "",

    @SerializedName("username")
    @Expose var username: String = "",

    @SerializedName("language_id")
    @Expose var languageId: String = "",

    @SerializedName("firstname")
    @Expose var firstname: String = "",

    @SerializedName("lastname")
    @Expose var lastname: String = "",

    @SerializedName("email")
    @Expose
    var email: String = "",

    @SerializedName("telephone")
    @Expose var telephone: String = "",

    @SerializedName("fax")
    @Expose var fax: String = "",

    @SerializedName("wishlist")
    @Expose var wishlist: List<String> = arrayListOf(),

    @SerializedName("newsletter")
    @Expose var newsletter: String = "",

    @SerializedName("address_id")
    @Expose var addressId: String = "",

    @SerializedName("ip")
    @Expose var ip: String = "",

    @SerializedName("status")
    @Expose var status: String = "",

    @SerializedName("safe")
    @Expose var safe: String = "",

    @SerializedName("code")
    @Expose var code: String = "",

    @SerializedName("date_added")
    @Expose var dateAdded: String = "",

    @SerializedName("seller_id")
    @Expose var sellerId: String = "",

    @SerializedName("tc_token")
    @Expose
    var tcToken: String = "",

    @SerializedName("rest_token")
    @Expose
    var restToken: String = "",

    @SerializedName("wishlist_total")
    @Expose var wishlistTotal: String = "",

    @SerializedName("cart_count_products")
    @Expose var cartCountProducts: Int = 0,

    @SerializedName("profile")
    @Expose var profile: String = ""
)