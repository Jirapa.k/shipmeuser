package co.th.shipmeuser.network.models.cart.searchSeller

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class GetSellerOrderRequest(
    @SerializedName("order_id")
    @Expose
    var orderId: String = ""
)