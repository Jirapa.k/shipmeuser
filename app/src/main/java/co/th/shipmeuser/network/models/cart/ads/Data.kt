package co.th.shipmeuser.network.models.cart.ads

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Data(
    @SerializedName("ads_type")
    @Expose
    var adsType: String = "",

    @SerializedName("ads_detail")
    @Expose
    var adsDetail: String = "",

    @SerializedName("ads_url")
    @Expose
    var adsUrl: String = "",

    @SerializedName("start_date")
    @Expose
    var startDate: String = "",

    @SerializedName("end_date")
    @Expose
    var endDate: String = "",

    @SerializedName("status")
    @Expose
    var status: String = ""
)