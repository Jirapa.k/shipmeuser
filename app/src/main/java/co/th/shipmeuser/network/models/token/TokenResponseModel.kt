package co.th.shipmeuser.network.models.token

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import co.th.shipmeuser.network.models.token.Data

data class TokenResponseModel(

    @SerializedName("success")
    @Expose var success: Int? = 0,

    @SerializedName("error")
    @Expose var error: List<String>? = listOf(),

    @SerializedName("data")
    @Expose var data: Data? = Data()
)