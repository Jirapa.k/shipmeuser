package co.th.shipmeuser.network.models.home

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Model(
    @SerializedName("name")
    @Expose var name: String = ""
)