package co.th.shipmeuser.network.models.home

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Categories(
    @SerializedName("category_id")
    @Expose var categoryId: Int = 0,

    @SerializedName("parent_id")
    @Expose var parentId: Int = 0,

    @SerializedName("name")
    @Expose var name: String = "",

    @SerializedName("seo_url")
    @Expose var seoUrl: String = "",

    @SerializedName("image")
    @Expose var image: String = "",

    @SerializedName("original_image")
    @Expose var originalImage: String = "",

    @SerializedName("categories")
    @Expose var categories: List<String> = arrayListOf()
)