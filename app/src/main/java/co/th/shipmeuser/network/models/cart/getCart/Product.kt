package co.th.shipmeuser.network.models.cart.getCart

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Product(
    @SerializedName("key")
    @Expose
    var key: String = "",

    @SerializedName("product_id")
    @Expose
    var productId: String = "",

    @SerializedName("image")
    @Expose
    var image: String = "",

    @SerializedName("image_color")
    @Expose
    var imageColor: String = "",

    @SerializedName("name")
    @Expose
    var name: String = "",

    @SerializedName("manufacturer")
    @Expose
    var manufacturer: String = "",

    @SerializedName("category")
    @Expose
    var category: String = "",

    @SerializedName("model")
    @Expose
    var model: String = "",

    @SerializedName("color")
    @Expose
    var color: String = "",

    @SerializedName("size")
    @Expose
    var size: String = "",

    @SerializedName("quantity")
    @Expose
    var quantity: String = "",

    @SerializedName("price")
    @Expose
    var price: String = "",

    @SerializedName("total")
    @Expose
    var total: String = "",

    @SerializedName("price_raw")
    @Expose
    var priceRaw: Int = 0,

    @SerializedName("total_raw")
    @Expose
    var totalRaw: Int = 0
)