package co.th.shipmeuser.network.models.products

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Datum(
    @SerializedName("product_id")
    @Expose
    var productId: String = "",

    @SerializedName("name")
    @Expose
    var name: String = "",

    @SerializedName("model")
    @Expose
    var model: String = "",

    @SerializedName("manufacturer_id")
    @Expose
    var manufacturerId: String = "",

    @SerializedName("manufacturer")
    @Expose
    var manufacturer: String = "",

    @SerializedName("category_id")
    @Expose
    var categoryId: String = "",

    @SerializedName("category_name")
    @Expose
    var categoryName: String = "",

    @SerializedName("description")
    @Expose
    var description: String = "",

    @SerializedName("image")
    @Expose
    var image: String = "",

    @SerializedName("price")
    @Expose
    var price: Int = 0,

    @SerializedName("color")
    @Expose
    var color: List<Color> = arrayListOf()
) : Parcelable