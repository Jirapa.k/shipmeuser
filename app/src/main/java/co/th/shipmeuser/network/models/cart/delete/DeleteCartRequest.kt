package co.th.shipmeuser.network.models.cart.delete

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class DeleteCartRequest(
    @SerializedName("key")
    @Expose
    var key: String = ""
)