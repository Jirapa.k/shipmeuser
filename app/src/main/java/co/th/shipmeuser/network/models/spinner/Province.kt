package co.th.shipmeuser.network.models.spinner

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Province(
    @SerializedName("zone_id")
    @Expose var zoneId: String? = null,

    @SerializedName("country_id")
    @Expose var countryId: String? = null,

    @SerializedName("name")
    @Expose var name: String? = null,

    @SerializedName("code")
    @Expose var code: String? = null,

    @SerializedName("status")
    @Expose var status: String? = null


)