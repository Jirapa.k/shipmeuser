package co.th.shipmeuser.network.models.products

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ProductResponse(
    @SerializedName("success")
    @Expose
    var success: Int = 0,

    @SerializedName("error")
    @Expose
    var error: List<String> = arrayListOf(),

    @SerializedName("data")
    @Expose
    var data: List<Datum> = arrayListOf()
):Parcelable