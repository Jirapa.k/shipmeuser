package co.th.shipmeuser.network.models.cart.comfirm

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Products(
    @SerializedName("key")
    @Expose
    var key: String = "",

    @SerializedName("product_id")
    @Expose
    var productId: String = "",

    @SerializedName("name")
    @Expose
    var name: String = "",

    @SerializedName("model")
    @Expose
    var model: String = "",

    @SerializedName("image")
    @Expose
    var image: String = "",

    @SerializedName("quantity")
    @Expose
    var quantity: String = "",

    @SerializedName("price")
    @Expose
    var price: String = "",

    @SerializedName("total")
    @Expose
    var total: String = ""
)