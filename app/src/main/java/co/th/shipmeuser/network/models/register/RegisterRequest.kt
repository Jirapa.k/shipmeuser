package co.th.shipmeuser.network.models.register

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class RegisterRequest(

    @SerializedName("username")
    @Expose
    var username: String = "",

    @SerializedName("firstname")
    @Expose
    var firstname: String = "",

    @SerializedName("lastname")
    @Expose
    var lastname: String = "",

    @SerializedName("email")
    @Expose
    var email: String = "",

    @SerializedName("password")
    @Expose
    var password: String = "",

    @SerializedName("telephone")
    @Expose
    var telephone: String = ""
)