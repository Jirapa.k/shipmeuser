package co.th.shipmeuser.network.models.cart.address

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class AddressRequest(
    @SerializedName("address")
    @Expose
    var address: String = "",

    @SerializedName("province")
    @Expose
    var province: String = "",

    @SerializedName("district")
    @Expose
    var district: String = "",

    @SerializedName("subdistrict")
    @Expose
    var subdistrict: String = "",

    @SerializedName("postcode")
    @Expose
    var postcode: String = ""
)