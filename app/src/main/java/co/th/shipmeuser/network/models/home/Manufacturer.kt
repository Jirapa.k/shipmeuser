package co.th.shipmeuser.network.models.home

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Manufacturer(
    @SerializedName("manufacturer_id")
    @Expose var manufacturerId: Int = 0,

    @SerializedName("name")
    @Expose var name: String = "",

    @SerializedName("image")
    @Expose var image: String = "",

    @SerializedName("original_image")
    @Expose var originalImage: String = "",

    @SerializedName("sort_order")
    @Expose var sortOrder: String = ""
)