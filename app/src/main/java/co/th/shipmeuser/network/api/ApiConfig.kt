package co.th.shipmeuser.network.api


import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

fun createCallService(): ApiInterface {
    val retrofit = Retrofit.Builder().run {
        baseUrl("https://shipme.gramickhouse.com/api/rest/")
        addConverterFactory(GsonConverterFactory.create())
        addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        build()
    }
    return retrofit.create(ApiInterface::class.java)
}