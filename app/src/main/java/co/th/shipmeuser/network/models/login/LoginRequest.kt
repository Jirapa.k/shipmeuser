package co.th.shipmeuser.network.models.login

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class LoginRequest(

    @SerializedName("email")
    @Expose var email: String = "",

    @SerializedName("password")
    @Expose var password: String = ""
)