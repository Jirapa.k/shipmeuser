package co.th.shipmeuser.network.models.cart.searchSeller

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Data(
    @SerializedName("order_id")
    @Expose
    var orderId: String = "",

    @SerializedName("seller_id")
    @Expose
    var sellerId: List<String> = arrayListOf()
)