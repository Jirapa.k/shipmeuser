package com.example.jirapakannasut.awareandroidtest.models

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResponseModel(

    @SerializedName("request_hash")
    @Expose var requestHash: String? = null,

    @SerializedName("request_cached")
    @Expose var requestCached: Boolean = false,

    @SerializedName("request_cache_expiry")
    @Expose var requestCacheExpiry: Int? = null,

    @SerializedName("characters")
    @Expose var characters: List<Character>? = null

) : Parcelable