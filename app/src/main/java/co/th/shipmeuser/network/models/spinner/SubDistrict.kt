package co.th.shipmeuser.network.models.spinner

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class SubDistrict(
    @SerializedName("subdistrict_id")
    @Expose var subdistrictId: String = "",

    @SerializedName("name")
    @Expose var name: String = "",

    @SerializedName("code")
    @Expose var code: String = "",

    @SerializedName("zip_code")
    @Expose var zipCode: String = ""
)