package co.th.shipmeuser.network.models.cart.comfirm

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Total(
    @SerializedName("title")
    @Expose
    var title: String = "",

    @SerializedName("text")
    @Expose
    var text: String = ""
)