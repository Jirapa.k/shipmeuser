package com.example.jirapakannasut.awareandroidtest.database

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface StudentDao {
    @Insert
    fun insertStudent(studentEntity: StudentEntity)

    @Update()
    fun updateStudent(studentEntity: StudentEntity)

    @Delete
    fun deleteStudent(studentEntity: StudentEntity)

    @Query("SELECT * FROM student")
    fun getStudentAll(): LiveData<List<StudentEntity>>

    @Query("SELECT * FROM student WHERE student.student_code = :studentCode")
    fun getStudentByCode(studentCode: Int): List<StudentEntity>

    @Query("SELECT * FROM student WHERE first_name LIKE :query")
    fun getStudentByQuery(query: String): List<StudentEntity>

    @Query("DELETE FROM student")
    fun deleteTable()
}