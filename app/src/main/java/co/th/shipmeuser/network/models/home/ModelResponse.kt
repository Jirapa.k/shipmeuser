package co.th.shipmeuser.network.models.home

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ModelResponse(
    @SerializedName("success")
    @Expose var success: Int? = 0,

    @SerializedName("error")
    @Expose var error: List<String>? = null,

    @SerializedName("data")
    @Expose var data: List<Model> = arrayListOf()
)