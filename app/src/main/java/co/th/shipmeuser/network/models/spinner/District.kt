package co.th.shipmeuser.network.models.spinner

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class District(
    @SerializedName("district_id")
    @Expose var districtId: String = "",

    @SerializedName("name")
    @Expose var name: String = "",

    @SerializedName("code")
    @Expose var code: String = ""
)