package co.th.shipmeuser.network.models.login

import co.th.shipmeuser.network.models.login.Data
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class LoginResponse(

    @SerializedName("success")
    @Expose var success: Int = 0,

    @SerializedName("error")
    @Expose var error: List<String> = arrayListOf(),

    @SerializedName("data")
    @Expose var data: Data = Data()
)