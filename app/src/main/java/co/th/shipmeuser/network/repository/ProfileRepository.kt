package co.th.shipmeuser.network.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import co.th.shipmeuser.network.api.ApiInterface
import co.th.shipmeuser.network.models.login.ErrorResponse
import co.th.shipmeuser.network.models.logout.LogoutResponse
import co.th.shipmeuser.network.models.profile.ProfileResponse
import co.th.shipmeuser.network.pref.pref.SharedPreference
import co.th.shipmeuser.utils.REST_TOKEN
import com.google.gson.Gson
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException

class ProfileRepository(
    private val client: ApiInterface,
    private val sharedPreferences: SharedPreference
) {
    private val profileResponse = MutableLiveData<ProfileResponse>()
    private val logutResponse = MutableLiveData<LogoutResponse>()
    private val authenToken =
        sharedPreferences.getValueString(REST_TOKEN)
    private val progressbarObservable = MutableLiveData<Boolean>()
    lateinit var disposable: Disposable

    fun getAccount(): LiveData<ProfileResponse> {
        disposable = client.getProfile(authenToken!!)
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { progressbarObservable.postValue(true) }
            .doOnComplete { progressbarObservable.postValue(false) }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                profileResponse.postValue(it)
            }, {
                progressbarObservable.postValue(false)
                if (it is HttpException) {
                    val errorJsonString = it.response().errorBody()?.string()
                    val response = Gson().fromJson(errorJsonString, ErrorResponse::class.java)
                    //response?.error?.get(0)?.let { it1 -> DialogUtils(context).alertDialog(it1) }
                    Log.e("error", "" + response)
                } else {
                    Log.e("error", it.message)
                }
            })
        return profileResponse
    }

    fun logout(): LiveData<LogoutResponse> {
        disposable = client.logout(authenToken!!)
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { progressbarObservable.postValue(true) }
            .doOnComplete { progressbarObservable.postValue(false) }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                logutResponse.postValue(it)
            }, {
                progressbarObservable.postValue(false)
                if (it is HttpException) {
                    val errorJsonString = it.response().errorBody()?.string()
                    val response = Gson().fromJson(errorJsonString, ErrorResponse::class.java)
                    //response?.error?.get(0)?.let { it1 -> DialogUtils(context).alertDialog(it1) }
                    Log.e("error", "" + response)
                } else {
                    Log.e("error", it.message)
                }
            })
        return logutResponse
    }

    fun progressDialog(): MutableLiveData<Boolean> {
        return progressbarObservable
    }

    fun disposeService() {
        disposable.dispose()
    }
}
