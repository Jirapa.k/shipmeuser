package co.th.shipmeuser.network.models.cart.getCart

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Currency(
    @SerializedName("currency_id")
    @Expose
    var currencyId: String = "",

    @SerializedName("symbol_left")
    @Expose
    var symbolLeft: String = "",

    @SerializedName("symbol_right")
    @Expose
    var symbolRight: String = ""
)