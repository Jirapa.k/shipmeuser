package co.th.shipmeuser.network.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import co.th.shipmeuser.network.api.ApiInterface
import co.th.shipmeuser.network.models.cart.AddCartRequest
import co.th.shipmeuser.network.models.cart.AddCartResponse
import co.th.shipmeuser.network.models.home.CategoriesResponse
import co.th.shipmeuser.network.models.home.ManufacturerResponse
import co.th.shipmeuser.network.models.home.ModelResponse
import co.th.shipmeuser.network.models.login.ErrorResponse
import co.th.shipmeuser.network.models.products.ProductResponse
import co.th.shipmeuser.network.pref.pref.SharedPreference
import co.th.shipmeuser.utils.REST_TOKEN
import com.google.gson.Gson
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException

class HomeRepository(
    private val client: ApiInterface,
    private val sharedPreferences: SharedPreference
) {
    private val categoriesResponse = MutableLiveData<CategoriesResponse>()
    private val typeResponse = MutableLiveData<ManufacturerResponse>()
    private val modelResponse = MutableLiveData<ModelResponse>()
    private val products = MutableLiveData<ProductResponse>()
    private val addCart = MutableLiveData<AddCartResponse>()
    private val authenToken =
        sharedPreferences.getValueString(REST_TOKEN)
    private val progressbarObservable = MutableLiveData<Boolean>()
    lateinit var disposable: Disposable

    fun getCategories(): LiveData<CategoriesResponse> {
        disposable = client.getCategories(authenToken!!)
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { progressbarObservable.postValue(true) }
            .doOnComplete { progressbarObservable.postValue(false) }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                categoriesResponse.postValue(it)
            }, {
                progressbarObservable.postValue(false)
                if (it is HttpException) {
                    val errorJsonString = it.response().errorBody()?.string()
                    val response = Gson().fromJson(errorJsonString, ErrorResponse::class.java)
                    //response?.error?.get(0)?.let { it1 -> DialogUtils(context).alertDialog(it1) }
                    Log.e("error", "" + response)
                } else {
                    Log.e("error", it.message)
                }
            })
        return categoriesResponse
    }

    fun getManufacturer(): LiveData<ManufacturerResponse> {
        disposable = client.getManufacturers(authenToken!!)
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { progressbarObservable.postValue(true) }
            .doOnComplete { progressbarObservable.postValue(false) }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                typeResponse.postValue(it)
            }, {
                progressbarObservable.postValue(false)
                if (it is HttpException) {
                    val errorJsonString = it.response().errorBody()?.string()
                    val response = Gson().fromJson(errorJsonString, ErrorResponse::class.java)
                    //response?.error?.get(0)?.let { it1 -> DialogUtils(context).alertDialog(it1) }
                    Log.e("error", "" + response)
                } else {
                    Log.e("error", it.message)
                }
            })
        return typeResponse
    }

    fun getModel(): LiveData<ModelResponse> {
        disposable = client.getModel(authenToken!!)
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { progressbarObservable.postValue(true) }
            .doOnComplete { progressbarObservable.postValue(false) }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                modelResponse.postValue(it)
            }, {
                progressbarObservable.postValue(false)
                if (it is HttpException) {
                    val errorJsonString = it.response().errorBody()?.string()
                    val response = Gson().fromJson(errorJsonString, ErrorResponse::class.java)
                    //response?.error?.get(0)?.let { it1 -> DialogUtils(context).alertDialog(it1) }
                    Log.e("error", "" + response)
                } else {
                    Log.e("error", it.message)
                }
            })
        return modelResponse
    }

    fun getProducts(): LiveData<ProductResponse> {
        disposable = client.getProducts(authenToken!!)
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { progressbarObservable.postValue(true) }
            .doOnComplete { progressbarObservable.postValue(false) }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                products.postValue(it)
            }, {
                progressbarObservable.postValue(false)
                if (it is HttpException) {
                    val errorJsonString = it.response().errorBody()?.string()
                    val response = Gson().fromJson(errorJsonString, ErrorResponse::class.java)
                    //response?.error?.get(0)?.let { it1 -> DialogUtils(context).alertDialog(it1) }
                    Log.e("error", "" + response)
                } else {
                    Log.e("error", it.message)
                }
            })
        return products
    }

    fun addToCard(addCartRequest: AddCartRequest): LiveData<AddCartResponse> {
        disposable = client.addToCard(authenToken!!, addCartRequest)
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { progressbarObservable.postValue(true) }
            .doOnComplete { progressbarObservable.postValue(false) }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                addCart.postValue(it)
            }, {
                progressbarObservable.postValue(false)
                if (it is HttpException) {
                    val errorJsonString = it.response().errorBody()?.string()
                    val response = Gson().fromJson(errorJsonString, ErrorResponse::class.java)
                    //response?.error?.get(0)?.let { it1 -> DialogUtils(context).alertDialog(it1) }
                    Log.e("error", "" + response)
                } else {
                    Log.e("error", it.message)
                }
            })
        return addCart
    }

    fun progressDialog(): MutableLiveData<Boolean> {
        return progressbarObservable
    }

    fun disposeService() {
        disposable.dispose()
    }
}
