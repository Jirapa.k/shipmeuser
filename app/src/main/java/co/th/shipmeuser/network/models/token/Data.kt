package co.th.shipmeuser.network.models.token

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Data(

    @SerializedName("access_token")
    @Expose var accessToken: String? = null,

    @SerializedName("expires_in")
    @Expose var expiresIn: Long? = 0,

    @SerializedName("token_type")
    @Expose var tokenType: String? = null
)