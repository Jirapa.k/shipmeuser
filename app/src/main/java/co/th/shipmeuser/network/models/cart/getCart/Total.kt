package co.th.shipmeuser.network.models.cart.getCart

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Total(
    @SerializedName("title")
    @Expose
    var title: String = "",

    @SerializedName("text")
    @Expose
    var text: String = "",

    @SerializedName("value")
    @Expose
    var value: Int = 0
)