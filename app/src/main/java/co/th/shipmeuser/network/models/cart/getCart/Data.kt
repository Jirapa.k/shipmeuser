package co.th.shipmeuser.network.models.cart.getCart

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Data(
    @SerializedName("products")
    @Expose
    var products: List<Product> = arrayListOf(),

    @SerializedName("totals")
    @Expose
    var totals: List<Total> = arrayListOf(),

    @SerializedName("total")
    @Expose
    var total: String = "",

    @SerializedName("total_raw")
    @Expose
    var totalRaw: Int = 0,

    @SerializedName("total_product_count")
    @Expose
    var totalProductCount: Int = 0,

    @SerializedName("currency")
    @Expose
    var currency: Currency = Currency()
)