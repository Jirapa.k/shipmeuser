package co.th.shipmeuser.network.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import co.th.shipmeuser.network.api.ApiInterface
import com.example.jirapakannasut.awareandroidtest.database.StudentDao
import com.example.jirapakannasut.awareandroidtest.database.StudentEntity
import com.example.jirapakannasut.awareandroidtest.models.ResponseModel
import io.reactivex.Observable
import retrofit2.Call
import retrofit2.Response

class ApiRepository(private val client: ApiInterface, private val studentDao: StudentDao) {

    private val responseModel = MutableLiveData<ResponseModel>()
//
//    fun getAnimeLiveData(): LiveData<ResponseModel> {
//        client.getAnimeLiveData().enqueue(object : retrofit2.Callback<ResponseModel> {
//            override fun onFailure(call: Call<ResponseModel>, t: Throwable) {
//                t.printStackTrace()
//            }
//
//            override fun onResponse(call: Call<ResponseModel>, response: Response<ResponseModel>) {
//                responseModel.postValue(response.body())
//            }
//        })
//        return responseModel
//    }

    fun delete(studentEntity: StudentEntity): Observable<Unit> {
        return Observable.fromCallable { studentDao.deleteStudent(studentEntity) }
    }

    fun update(studentEntity: StudentEntity): Observable<Unit> {
        return Observable.fromCallable { studentDao.updateStudent(studentEntity) }
    }

    fun insert(studentEntity: StudentEntity): Observable<Unit> {
        return Observable.fromCallable { studentDao.insertStudent(studentEntity) }
    }

    fun search(query: String): List<StudentEntity>? {
        return studentDao.getStudentByQuery(query)
    }

    fun getStudentList(): LiveData<List<StudentEntity>> {
        return studentDao.getStudentAll()
    }
}

