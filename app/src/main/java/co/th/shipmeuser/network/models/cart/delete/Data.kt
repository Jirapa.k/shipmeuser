package co.th.shipmeuser.network.models.cart.delete

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Data(
    @SerializedName("total")
    @Expose
    var total: String = "",

    @SerializedName("total_product_count")
    @Expose
    var totalProductCount: Int = 0,

    @SerializedName("total_price")
    @Expose
    var totalPrice: String = ""
)