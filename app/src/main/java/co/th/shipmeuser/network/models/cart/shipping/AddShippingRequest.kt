package co.th.shipmeuser.network.models.cart.shipping

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class AddShippingRequest(
    @SerializedName("shipping_method")
    @Expose
    var shippingMethod: String = "",

    @SerializedName("code")
    @Expose
    var code: String = ""
)