package co.th.shipmeuser.network.models.cart

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Product(
    @SerializedName("product_id")
    @Expose
    var productId: String = "",

    @SerializedName("name")
    @Expose
    var name: String = "",

    @SerializedName("quantity")
    @Expose
    var quantity: String = ""
)