package co.th.shipmeuser.network.models.cart

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class AddCartRequest(
    @SerializedName("product_id")
    @Expose
    var productId: String = "",

    @SerializedName("quantity")
    @Expose
    var quantity: String = ""
)