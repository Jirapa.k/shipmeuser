package co.th.shipmeuser.network.api

import co.th.shipmeuser.network.models.AddDataSuccess
import co.th.shipmeuser.network.models.cart.AddCartRequest
import co.th.shipmeuser.network.models.cart.AddCartResponse
import co.th.shipmeuser.network.models.cart.address.AddressRequest
import co.th.shipmeuser.network.models.cart.ads.AdsSearchResponse
import co.th.shipmeuser.network.models.cart.comfirm.ConfirmResponse
import co.th.shipmeuser.network.models.cart.delete.DeleteCartRequest
import co.th.shipmeuser.network.models.cart.delete.DeleteCartResponse
import co.th.shipmeuser.network.models.cart.getCart.GetCartResponse
import co.th.shipmeuser.network.models.cart.payment.PaymentRequest
import co.th.shipmeuser.network.models.cart.searchSeller.GetSellerOrderRequest
import co.th.shipmeuser.network.models.cart.searchSeller.GetSellerResponse
import co.th.shipmeuser.network.models.cart.shipping.AddShippingRequest
import co.th.shipmeuser.network.models.home.CategoriesResponse
import co.th.shipmeuser.network.models.home.ManufacturerResponse
import co.th.shipmeuser.network.models.home.ModelResponse
import co.th.shipmeuser.network.models.login.LoginRequest
import co.th.shipmeuser.network.models.login.LoginResponse
import co.th.shipmeuser.network.models.logout.LogoutResponse
import co.th.shipmeuser.network.models.products.ProductResponse
import co.th.shipmeuser.network.models.profile.ProfileResponse
import co.th.shipmeuser.network.models.register.RegisterRequest
import co.th.shipmeuser.network.models.register.RegisterResponse
import co.th.shipmeuser.network.models.spinner.CountriesRequest
import co.th.shipmeuser.network.models.spinner.CountriesResponse
import co.th.shipmeuser.network.models.spinner.DistrictResponse
import co.th.shipmeuser.network.models.spinner.SubDistrictResponse
import co.th.shipmeuser.network.models.token.TokenResponseModel
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST

interface ApiInterface {

    //    @GET("characters_staff")
//    fun getAnimeLiveData(): Call<ResponseModel>

    @POST("gettoken")
    fun getToken(@Header("Authorization") authorization: String): Observable<TokenResponseModel>

    @POST("register")
    fun register(@Header("Authorization") authorization: String, @Body registerRequest: RegisterRequest): Observable<RegisterResponse>

    @POST("categories")
    fun getCategories(@Header("Authorization") authorization: String): Observable<CategoriesResponse>

    @POST("manufacturers")
    fun getManufacturers(@Header("Authorization") authorization: String): Observable<ManufacturerResponse>

    @POST("getModel")
    fun getModel(@Header("Authorization") authorization: String): Observable<ModelResponse>

    @POST("login")
    fun login(@Header("Authorization") authorization: String, @Header("x-oc-apps") role: String, @Body loginRequest: LoginRequest): Observable<LoginResponse>

    @POST("products")
    fun getProducts(@Header("Authorization") authorization: String): Observable<ProductResponse>

    @POST("countries")
    fun getProvince(@Header("Authorization") authorization: String, @Body countriesRequest: CountriesRequest): Observable<CountriesResponse>

    @POST("zones")
    fun getDistrict(@Header("Authorization") authorization: String, @Body countriesRequest: CountriesRequest): Observable<DistrictResponse>

    @POST("districts")
    fun getSubDistrict(@Header("Authorization") authorization: String, @Body countriesRequest: CountriesRequest): Observable<SubDistrictResponse>

    @POST("getaccountdetail")
    fun getProfile(@Header("Authorization") authorization: String): Observable<ProfileResponse>

    @POST("logout")
    fun logout(@Header("Authorization") authorization: String): Observable<LogoutResponse>

    @POST("addcart")
    fun addToCard(@Header("Authorization") authorization: String, @Body addCartRequest: AddCartRequest): Observable<AddCartResponse>

    @POST("cart")
    fun getCart(@Header("Authorization") authorization: String): Observable<GetCartResponse>

    @POST("deletecart")
    fun deleteCart(@Header("Authorization") authorization: String, @Body deleteCartRequest: DeleteCartRequest): Observable<DeleteCartResponse>

    @POST("paymentaddress")
    fun paymentAddress(@Header("Authorization") authorization: String, @Body addressRequest: AddressRequest): Observable<AddDataSuccess>

    @POST("addshippingmethods")
    fun addShippingMethods(@Header("Authorization") authorization: String, @Body addShippingRequest: AddShippingRequest): Observable<AddDataSuccess>

    @POST("addpaymentmethods")
    fun addPaymentMethod(@Header("Authorization") authorization: String, @Body paymentRequest: PaymentRequest): Observable<AddDataSuccess>

    @POST("confirm")
    fun confirmOrder(@Header("Authorization") authorization: String): Observable<ConfirmResponse>

    @POST("getsellerordershop")
    fun getSellerOrderShop(@Header("Authorization") authorization: String, @Body getSellerOrderRequest: GetSellerOrderRequest): Observable<GetSellerResponse>

    @POST("getbannerstoresearch")
    fun getBannerStoreSearch(@Header("Authorization") authorization: String): Observable<AdsSearchResponse>

}