package com.example.jirapakannasut.awareandroidtest.models

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Character(

    @SerializedName("mal_id")
    @Expose var malId: Int? = null,

    @SerializedName("url")
    @Expose var url: String? = null,

    @SerializedName("image_url")
    @Expose var imageUrl: String? = null,

    @SerializedName("name")
    @Expose var name: String? = null,

    @SerializedName("role")
    @Expose var role: String? = null,

    @SerializedName("voice_actors")
    @Expose var voiceActors: List<VoiceActor>? = null

) : Parcelable