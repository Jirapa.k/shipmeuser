package co.th.shipmeuser.network.models.cart.ads

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class AdsSearchResponse(
    @SerializedName("success")
    @Expose
    var success: Int = 0,

    @SerializedName("error")
    @Expose
    var error: List<String> = arrayListOf(),

    @SerializedName("data")
    @Expose
    var data: Data = Data()
)