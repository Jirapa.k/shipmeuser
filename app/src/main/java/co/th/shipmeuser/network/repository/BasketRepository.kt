package co.th.shipmeuser.network.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import co.th.shipmeuser.network.api.ApiInterface
import co.th.shipmeuser.network.models.AddDataSuccess
import co.th.shipmeuser.network.models.cart.address.AddressRequest
import co.th.shipmeuser.network.models.cart.comfirm.ConfirmResponse
import co.th.shipmeuser.network.models.cart.delete.DeleteCartRequest
import co.th.shipmeuser.network.models.cart.delete.DeleteCartResponse
import co.th.shipmeuser.network.models.cart.getCart.GetCartResponse
import co.th.shipmeuser.network.models.cart.payment.PaymentRequest
import co.th.shipmeuser.network.models.cart.searchSeller.GetSellerOrderRequest
import co.th.shipmeuser.network.models.cart.searchSeller.GetSellerResponse
import co.th.shipmeuser.network.models.cart.shipping.AddShippingRequest
import co.th.shipmeuser.network.models.login.ErrorResponse
import co.th.shipmeuser.network.models.spinner.CountriesRequest
import co.th.shipmeuser.network.models.spinner.CountriesResponse
import co.th.shipmeuser.network.models.spinner.DistrictResponse
import co.th.shipmeuser.network.models.spinner.SubDistrictResponse
import co.th.shipmeuser.network.pref.pref.SharedPreference
import co.th.shipmeuser.utils.REST_TOKEN
import com.google.gson.Gson
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException

class BasketRepository(
    private val client: ApiInterface,
    private val sharedPreferences: SharedPreference
) {
    private val getCartResponse = MutableLiveData<GetCartResponse>()
    private val deleteCartResponse = MutableLiveData<DeleteCartResponse>()
    private val countriesModel = MutableLiveData<CountriesResponse>()
    private val districtModel = MutableLiveData<DistrictResponse>()
    private val subDistrictModel = MutableLiveData<SubDistrictResponse>()
    private val addDataSuccess = MutableLiveData<AddDataSuccess>()
    private val confirmResponse = MutableLiveData<ConfirmResponse>()
    private val getSellerResponse = MutableLiveData<GetSellerResponse>()
    private val authenToken =
        sharedPreferences.getValueString(REST_TOKEN)

    lateinit var disposable: Disposable

    fun getCart(): LiveData<GetCartResponse> {
        disposable = client.getCart(authenToken!!)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                getCartResponse.postValue(it)
            }, {
                if (it is HttpException) {
                    val errorJsonString = it.response().errorBody()?.string()
                    val response = Gson().fromJson(errorJsonString, ErrorResponse::class.java)
                    //response?.error?.get(0)?.let { it1 -> DialogUtils(context).alertDialog(it1) }
                    Log.e("error", "" + response)
                } else {
                    Log.e("error", it.message)
                }
            })
        return getCartResponse
    }

    fun deletCart(deleteCartRequest: DeleteCartRequest): LiveData<DeleteCartResponse> {
        disposable = client.deleteCart(authenToken!!, deleteCartRequest)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                deleteCartResponse.postValue(it)
            }, {
                if (it is HttpException) {
                    val errorJsonString = it.response().errorBody()?.string()
                    val response = Gson().fromJson(errorJsonString, ErrorResponse::class.java)
                    //response?.error?.get(0)?.let { it1 -> DialogUtils(context).alertDialog(it1) }
                    Log.e("error", "" + response)
                } else {
                    Log.e("error", it.message)
                }
            })
        return deleteCartResponse
    }

    fun getCountries(countriesRequest: CountriesRequest): LiveData<CountriesResponse> {
        disposable = client.getProvince(authenToken!!, countriesRequest)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                countriesModel.postValue(it)
            }, {
                if (it is HttpException) {
                    val errorJsonString = it.response().errorBody()?.string()
                    val response = Gson().fromJson(errorJsonString, ErrorResponse::class.java)
                    //response?.error?.get(0)?.let { it1 -> DialogUtils(context).alertDialog(it1) }
                    Log.e("error", "" + response)
                } else {
                    Log.e("error", it.message)
                }
            })
        return countriesModel
    }

    fun getDistrict(countriesRequest: CountriesRequest): LiveData<DistrictResponse> {
        disposable = client.getDistrict(authenToken!!, countriesRequest)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                districtModel.postValue(it)
            }, {
                if (it is HttpException) {
                    val errorJsonString = it.response().errorBody()?.string()
                    val response = Gson().fromJson(errorJsonString, ErrorResponse::class.java)
                    //response?.error?.get(0)?.let { it1 -> DialogUtils(context).alertDialog(it1) }
                    Log.e("error", "" + response)
                } else {
                    Log.e("error", it.message)
                }
            })
        return districtModel
    }

    fun getSubDistrict(countriesRequest: CountriesRequest): LiveData<SubDistrictResponse> {
        disposable = client.getSubDistrict(authenToken!!, countriesRequest)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                subDistrictModel.postValue(it)
            }, {
                if (it is HttpException) {
                    val errorJsonString = it.response().errorBody()?.string()
                    val response = Gson().fromJson(errorJsonString, ErrorResponse::class.java)
                    //response?.error?.get(0)?.let { it1 -> DialogUtils(context).alertDialog(it1) }
                    Log.e("error", "" + response)
                } else {
                    Log.e("error", it.message)
                }
            })
        return subDistrictModel
    }

    fun address(addressRequest: AddressRequest): LiveData<AddDataSuccess> {
        disposable = client.paymentAddress(authenToken!!, addressRequest)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                addDataSuccess.postValue(it)
            }, {
                if (it is HttpException) {
                    val errorJsonString = it.response().errorBody()?.string()
                    val response = Gson().fromJson(errorJsonString, ErrorResponse::class.java)
                    //response?.error?.get(0)?.let { it1 -> DialogUtils(context).alertDialog(it1) }
                    Log.e("error", "" + response)
                } else {
                    Log.e("error", it.message)
                }
            })
        return addDataSuccess
    }

    fun addShippingMethod(addShippingRequest: AddShippingRequest): LiveData<AddDataSuccess> {
        disposable = client.addShippingMethods(authenToken!!, addShippingRequest)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                addDataSuccess.postValue(it)
            }, {
                it.printStackTrace()
                Log.e("error", it.localizedMessage!!)
            })
        return addDataSuccess
    }

    fun addPaymentMethod(paymentRequest: PaymentRequest): LiveData<AddDataSuccess> {
        disposable = client.addPaymentMethod(authenToken!!, paymentRequest)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                addDataSuccess.postValue(it)
            }, {
                if (it is HttpException) {
                    val errorJsonString = it.response().errorBody()?.string()
                    val response = Gson().fromJson(errorJsonString, ErrorResponse::class.java)
                    //response?.error?.get(0)?.let { it1 -> DialogUtils(context).alertDialog(it1) }
                    Log.e("error", "" + response)
                } else {
                    Log.e("error", it.message)
                }
            })
        return addDataSuccess
    }

    fun confirmOrder(): LiveData<ConfirmResponse> {
        disposable = client.confirmOrder(authenToken!!)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                confirmResponse.postValue(it)
            }, {
                if (it is HttpException) {
                    val errorJsonString = it.response().errorBody()?.string()
                    val response = Gson().fromJson(errorJsonString, ErrorResponse::class.java)
                    //response?.error?.get(0)?.let { it1 -> DialogUtils(context).alertDialog(it1) }
                    Log.e("error", "" + response)
                } else {
                    Log.e("error", it.message)
                }
            })
        return confirmResponse
    }

    fun getSellerOrder(getSellerOrderRequest: GetSellerOrderRequest): LiveData<GetSellerResponse> {
        disposable = client.getSellerOrderShop(authenToken!!, getSellerOrderRequest)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                getSellerResponse.postValue(it)
            }, {
                if (it is HttpException) {
                    val errorJsonString = it.response().errorBody()?.string()
                    val response = Gson().fromJson(errorJsonString, ErrorResponse::class.java)
                    //response?.error?.get(0)?.let { it1 -> DialogUtils(context).alertDialog(it1) }
                    Log.e("error", "" + response)
                } else {
                    Log.e("error", it.message)
                }
            })
        return getSellerResponse
    }
}
