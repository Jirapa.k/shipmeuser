package co.th.shipmeuser.utils

const val TOKEN = "token"
const val TOKEN_TYPE = "tokenType"
const val REST_TOKEN = "tokenType"
const val PRODUCT_MODEL = "productModel"
const val CASH_ON_DELIVERY = "cod"
const val BANKING = "bank_transfer"
const val CREDIT_CARD = "credit_card"
const val ORDER_ID = "order_id"